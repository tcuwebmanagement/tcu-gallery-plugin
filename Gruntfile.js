/*eslint no-undef: "error"*/
/*eslint-env node*/

module.exports = function( grunt ) {
    grunt.initConfig( {

        // Let's zip up our /dist (production wordpress theme)
        // Change version number in style.css
        compress: {
            main: {
                options: {
                    archive: 'tcu-gallery-plugin.1.0.3.zip'
                },
                files: [
                    {
                        expand: true,
                        cwd: 'dist/',
                        src: [ '**' ],
                        dest: 'tcu-gallery-plugin/'
                    }
                ]
            }
        }
    } );

    grunt.loadNpmTasks( 'grunt-contrib-compress' );

    // Development workflow
    grunt.registerTask( 'default', [ 'compress' ] );

    // Zip our clean directory to easily install in WP
    grunt.registerTask( 'zip', [ 'compress' ] );
};
