<?php
/**
 * Display our gallery.
 *
 * @package tcu_gallery_plugin
 * @since TCU Gallery Plugin 1.0.0
 */

/**
 * Exit if accessed directly
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/**
 * Display our gallery
 *
 * @uses Tcu_Create_Gallery
 */
class Tcu_Display_Gallery {

	/**
	 * Keep track of the collection slug so we don't repeat the slider_html method.
	 *
	 * @var array $collection_slugs The collection term slug.
	 */
	private $collection_slugs = [];

	/**
	 * Our Constructor
	 */
	public function __construct() {

			// [tcu_gallery id="1"]
			add_shortcode( 'tcu_gallery', array( $this, 'create_shortcode' ) );

			// Print thickbox.
			add_action( 'admin_footer', array( $this, 'media_thickbox' ) );

			// Print media button.
			add_action( 'media_buttons', array( $this, 'media_button' ), 999 );

			// Set up thumbnail.
			add_action( 'init', array( $this, 'set_new_image_size' ) );
	}

	/**
	 * Let's get our object set as an array
	 *
	 * @param  object $data   gallery object.
	 * @return array  gallery object in array
	 */
	public static function set_array( $data ) {
		foreach ( $data as $key => $value ) {
			$key = $value;
		}

		return $data;
	}

	/**
	 * Query tcu_gallery post type
	 *
	 * @param array $args     Arguments to be merged with the post query.
	 * @return object WP_Query Post query
	 */
	public static function query_galleries( $args = array() ) {
		$gallery = array();

		// Our default query arguments.
		$defaults = array(
			'posts_per_page' => -1,
			'offset'         => 0,
			'post_status'    => 'publish',
			'post_type'      => Tcu_Create_Gallery::get_post_type_name(),
			'orderby'        => 'ID',
			'order'          => 'ASC',
			'no_found_rows'  => true,
		);

		// Merge the query arguments.
		$args = array_merge( (array) $defaults, (array) $args );

		// Execute the query.
		$query = get_posts( $args );

		return $query;
	}

	/**
	 * Get all our galleries
	 *
	 * @return array $output All our galleries
	 **/
	public static function all_galleries() {

		// Query our post type.
		$galleries = self::query_galleries();

		$output = array();

		// Loop through each gallery.
		foreach ( $galleries as $gallery ) {

			// Set array.
			$gallery  = self::set_array( get_object_vars( $gallery ) );
			$output[] = $gallery;
		}

		wp_reset_postdata();

		return $output;
	}


	/**
	 * Grab our gallery by post ID
	 *
	 * @param int $id The gallery/post ID.
	 * @return array $output Gallery by ID.
	 */
	public function get_by_id( $id ) {
		$post = get_post( $id );

		// Check if post exists.
		if ( ! $post ) {
			return false;
		}

		// Check if we are using the correct post type.
		if ( Tcu_Create_Gallery::get_post_type_name() !== $post->post_type ) {
			return false;
		}

		$output = array();

		// Set up our array.
		$gallery  = self::set_array( get_object_vars( $post ) );
		$output[] = $gallery;

		return $output;
	}

	/**
	 * Query attachment by collection name.
	 *
	 * @param string $term_slug The slug of the taxonomy term.
	 */
	public function get_images_by_collection( $term_slug ) {
		$collections = get_terms( array(
			'taxonomy'   => Tcu_Create_Gallery::get_collections_name(),
			'hide_empty' => true,
		) );

		$args = array(
			'post_status' => 'inherit',
			'post_type'   => 'attachment',
			'tax_query'   => array(
				array(
					'taxonomy' => Tcu_Create_Gallery::get_collections_name(),
					'field'    => 'slug',
					'terms'    => $term_slug,
				),
			),
		);

		$collection_query = self::query_galleries( $args );

		return $collection_query;

	}

	/**
	 * Create a new array with collection and filter term information.
	 *
	 * @param string $id The post ID for our gallery.
	 */
	private function gallery_data( $id ) {

		// All images in current Gallery.
		$images = get_field( 'tcu_gallery_add_images', $id );

		// Find all collection terms that are not empty.
		$collections = get_terms( array(
			'taxonomy'   => Tcu_Create_Gallery::get_collections_name(),
			'hide_empty' => true,
		) );

		// Find all filter terms that are not empty.
		$filters = get_terms( array(
			'taxonomy'   => Tcu_Create_Gallery::get_filters_name(),
			'hide_empty' => true,
		) );

		$data     = [];
		$data_set = [];

		// Loop through images.
		foreach ( $images as $image ) {

			$data = array(
				'ID'                  => $image['ID'],
				'alt'                 => $image['alt'],
				'description'         => $image['description'],
				'title'               => $image['title'],
				'url'                 => $image['url'],
				'gallery_thumbnail'   => $image['sizes']['gallery_thumbnail'],
				'gallery_thumbnail_w' => $image['sizes']['gallery_thumbnail-width'],
				'gallery_thumbnail_h' => $image['sizes']['gallery_thumbnail-height'],
				'gallery_fullwidth'   => $image['sizes']['gallery_fullwidth'],
				'gallery_fullwidth_w' => $image['sizes']['gallery_fullwidth-width'],
				'gallery_fullwidth_h' => $image['sizes']['gallery_fullwidth-height'],
				'has_collection'      => false,
				'filters'             => false,
			);

			// Collection term for each image post.
			$collection_term = wp_get_post_terms( $image['ID'], Tcu_Create_Gallery::get_collections_name() );
			$filter_terms    = wp_get_post_terms( $image['ID'], Tcu_Create_Gallery::get_filters_name() );

			if ( ! empty( $collection_term ) ) {
				$key = key( $collection_term );

				$data['has_collection']  = true;
				$data['collection_info'] = array(
					'collection_slug' => $collection_term[ $key ]->slug,
					'collection_name' => $collection_term[ $key ]->name,
				);
			}

			if ( ! empty( $filter_terms ) ) {
				$data['filters'] = $filter_terms;
			}

			$data_set[] = $data;
		}

		return $data_set;
	}

	/**
	 * Return first element in array.
	 *
	 * @param array $data_set An array.
	 */
	private function get_first_item( array $data_set ) {

		foreach ( $data_set as $first ) {

			return $first;
		}

		return null;
	}

	/**
	 * Return array of images in a collection term.
	 *
	 * @param array  $data_set        The array  of images.
	 * @param string $collection_term The collection term name.
	 */
	private function get_collection( $data_set, $collection_term ) {

		$data_set_length = count( $data_set );
		$new_array       = [];

		for ( $i = 0; $i < $data_set_length; $i++ ) {

			if (
				true === $data_set[ $i ]['has_collection'] &&
				$data_set[ $i ]['collection_info']['collection_slug'] === $collection_term
			) {
				$new_array[] = $data_set[ $i ];
			}
		}

		return $new_array;
	}

	/**
	 * Return an array with all the filters in our Gallery post by ID.
	 *
	 * Ensure that we only display the filters for each Gallery instead
	 * of ALL the filter terms in the taxonmy.
	 *
	 * @param string $id The ID of the Gallery post.
	 */
	public function get_filters_by_gallery( $id ) {
		$data    = $this->gallery_data( $id );
		$filters = [];

		foreach ( $data as $image ) {

			if ( $image['filters'] ) {

				foreach ( $image['filters'] as $filter ) {

					if ( in_array( $filter->slug, $filters, true ) ) {
						return;
					}

					$filters[ $filter->slug ] = array(
						'slug' => $filter->slug,
						'name' => $filter->name,
					);
				}
			}
		}

		return $filters;
	}

	/**
	 * Let's keep track of a collection term so the slider_html method only runs once.
	 *
	 * @param array $term The term slug in the collection taxonomy.
	 */
	private function has_term_slug( $term ) {

		// Check if term already exists.
		if ( in_array( $term, $this->collection_slugs, true ) ) {
			return true;
		}

		// If not, let's add it into our array.
		$this->collection_slugs[] = $term;

		return false;
	}

	/**
	 * Display Slider.
	 *
	 * @param array $collection_images Set of images in a collection.
	 */
	private function slider_html( $collection_images ) {

		// Find first item in a collection.
		$collection_info = $this->get_first_item( $collection_images );
		$collection_name = $collection_info['collection_info']['collection_name'];
		$image_count     = count( $collection_images );

		$html = '<div class="tcu-gallery-slider"><ul class="slides">';

		// Make sure we don't lazy load the first image.
		$html .= '<li>
					<a href="' . $collection_images[0]['gallery_fullwidth'] . '" title="' . $collection_images[0]['title'] . '">
						<img width="' . $collection_images[0]['gallery_thumbnail_w'] . '" height="' . $collection_images[0]['gallery_thumbnail_h'] . '" src="' . $collection_images[0]['gallery_thumbnail'] . '"  alt="' . $collection_images[0]['alt'] . '">
					</a>
				</li>';

		for ( $i = 1; $i < $image_count; $i++ ) {
			$html .= '<li>
				<a href="' . $collection_images[ $i ]['gallery_fullwidth'] . '" title="' . $collection_images[ $i ]['title'] . '">
					<img width="' . $collection_images[ $i ]['gallery_thumbnail_w'] . '" height="' . $collection_images[ $i ]['gallery_thumbnail_h'] . '" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" class="tcu-slider-lazy" data-src="' . $collection_images[ $i ]['gallery_thumbnail'] . '"  alt="' . $collection_images[ $i ]['alt'] . '">
				</a>
			</li>';
		}

		$html .= '</ul></div>';

		$html .= '<div class="tcu-gallery-hover">';
		// Icon.
		$html .= '<div class="tcu-action-btn">
			<a href="#" title="' . $collection_name . '">
				<i class="tcu-icon-collection"></i>
			</a>
		</div><!-- end of .tcu-action-btn -->';
		// Description.
		$html .= '<div class="tcu-gallery-description">
			<h4>
				<a href="#" title="' . $collection_name . '">' . $collection_name . '</a>
			</h4>
		</div><!-- end of .tcu-gallery-description -->';
		$html .= '</div><!-- end of .tcu-gallery-hover -->';

		return $html;
	}

	/**
	 * Display filter navigation
	 *
	 * @param string $id The ID of the Gallery post.
	 */
	private function display_filters( $id ) {
		$filters = $this->get_filters_by_gallery( $id );

		$html = '<div class="tcu-top32 tcu-below32">
					<button aria-expanded="true" class="tcu-filter-heading tcu-mar-b0 h3" type="button">
						Filters
						<span class="tcu-visuallyhidden">Contract Menu</span>
						<span> &#8722; </span>
					</button>
					<ul class="tcu-gallery-filter tcu-alignc">
						<li>
							<a aria-label="View all images" href="#" data-filter="*">View All</a>
						</li>';

		// Loop through filters.
		foreach ( $filters as $filter ) {
			$html .= '<li>
						<a aria-label="' . $filter['name'] . ' - Filter images by ' . $filter['name'] . '" href="#" data-filter=".' . $filter['slug'] . '">' . $filter['name'] . '</a>
					  </li>';
		}

		$html .= '</ul></div>';

		return $html;
	}

	/**
	 * Grab filter slug and use it as the class name.
	 *
	 * @param string $id The image ID.
	 */
	private function get_filter_slug( $id ) {

		$filters = get_field( 'tcu_gallery_filters', $id );
		$html    = '';

		if ( $filters ) {
			// Add class name to add into tcu-gallery-item.
			foreach ( $filters as $filter ) {
				// Make sure we add a space at the end to seperate the class names in the HTML.
				$html .= $filter->slug . ' ';
			}
		}

		return $html;
	}

	/**
	 * Render our Gallery's HTML
	 *
	 * @param array $gallery WP_Query of our gallery by ID.
	 * @return string $html The HTML shell
	 */
	public function render_html( $gallery ) {
		$key             = key( $gallery );
		$id              = $gallery[ $key ]['ID'];
		$images          = $this->gallery_data( $id );
		$display_filters = get_field( 'tcu_gallery_display_filter', $id );

		// Start our HTML wrapper.
		$html = '';

		// Display Filters.
		if ( $display_filters ) {
			$html .= $this->display_filters( $id );
		}

		// Start Gallery.
		$html .= '<div id="tcu-gallery-wrapper" class="tcu-gallery-wrapper size1of1 m-size1of1 tcu-pad-lr0 tcu-pad-tb0 cf">';

		// Loop through images in Gallery.
		foreach ( $images as $image ) {

			// If image contains a collection term then it's going to go into a slider.
			if ( $image['has_collection'] ) {
				$collection_term = $image['collection_info']['collection_slug'];

				// Check if we have already created a slider for this term.
				if ( ! $this->has_term_slug( $collection_term ) ) {

					// Grab array with images in collection term.
					$images_in_collection = $this->get_collection( $images, $collection_term );

					// Start the tcu-gallery-item.
					$html .= '<div class="tcu-gallery-item ';

					// This adds a filter slug as a CSS class name.
					$html .= $this->get_filter_slug( $image['ID'] );
					$html .= '  tcu-pad-lr0 tcu-pad-tb0 tcu-gallery-size1of3 tcu-gallery-m-size1of2">';

					// Start thumbnail wrapper.
					$html .= '<div class="tcu-gallery-thumb">';

					// Create Slider.
					$html .= $this->slider_html( $images_in_collection );

					$html .= '</div><!-- end of .tcu-gallery-thumb -->';
					$html .= '</div><!-- end of .tcu-gallery-item -->';
				}
				// Else, we will add it into an image tag.
			} else {

				// Start the tcu-gallery-item.
				$html .= '<div class="tcu-gallery-item ';

				// This adds a filter slug as a CSS class name.
				$html .= $this->get_filter_slug( $image['ID'] );
				$html .= '  tcu-pad-lr0 tcu-pad-tb0 tcu-gallery-size1of3 tcu-gallery-m-size1of2">';

				// Start thumbnail wrapper.
				$html .= '<div class="tcu-gallery-thumb">';

				$html .= '<img width="' . $image['gallery_thumbnail_w'] . '" height="' . $image['gallery_thumbnail_h'] . '" src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="  class="tcu-gallery-lazy" data-src="' . $image['gallery_thumbnail'] . '" alt="' . $image['alt'] . '" />';
				$html .= '<div class="tcu-gallery-hover">';
				// Icon.
				$html .= '<div class="tcu-action-btn">
					<a href="' . $image['gallery_fullwidth'] . '" class="tcu-popup-link--icon" title="' . $image['description'] . '">
						<i class="tcu-icon-magnifier"></i>
					</a>
				</div><!-- end of .tcu-action-btn -->';
				// Description.
				$html .= '<div class="tcu-gallery-description">
					<h4>
						<a href="' . $image['gallery_fullwidth'] . '" class="tcu-popup-link" title="' . $image['description'] . '">' . $image['title'] . '</a>
					</h4>
				</div><!-- end of .tcu-gallery-description -->';
				$html .= '</div><!-- end of .tcu-gallery-hover -->';

				$html .= '</div><!-- end of .tcu-gallery-thumb -->';
				$html .= '</div><!-- end of .tcu-gallery-item -->';
			}
		}

		$html .= '</div><!-- .gallery wrapper -->';

		return $html;

	}

	/**
	 * Creates our shortcode
	 *
	 * @param array $atts The gallery ID.
	 * @return string $html Gallery shortcode
	 */
	public function create_shortcode( $atts ) {
		if ( ! empty( $atts['id'] ) ) {
			$gallery = self::get_by_id( $atts['id'] );
			$html    = self::render_html( $gallery );
		}

		return $html;
	}

	/**
	 * Prints our "ADD Gallery" Media Thickbox
	 *
	 * @return void
	 */
	public function media_thickbox() {
		global $pagenow;

		if ( ( 'post.php' || 'post-new.php' ) != $pagenow ) {
			return;
		}

		// Let's get all our galleries.
		$galleries = self::all_galleries(); ?>

		<style type="text/css">
			.section {
				padding: 15px 15px 0 15px;
			}
		</style>

		<script type="text/javascript">
		/**
		* Send shortcode to the editor
		*/
		var insertGallery = function() {

			var gallery_id = jQuery( '#tcugalleries_thickbox' ).val();

			// alert if no gallery was selected
			if( '-1' === gallery_id ) {
				return alert( 'Please select a gallery from the dropdown menu.' );
			}

			// Send shortcode to editor
			send_to_editor( '[tcu_gallery id="' + gallery_id + '"]' );

			// close thickbox
			tb_remove();
		};
		</script>

		<style>
		.acf-field[data-name="tcu_gallery_description"] a.insert-gallery {
			display: none !important;
		}
		</style>
		<div id="select-tcu-gallery" style="display: none;">
			<div class="section">
				<h2><?php esc_html_e( 'Add a gallery', 'tcu-gallery-plugin' ); ?></h2>
				<span><?php esc_html_e( 'Select a gallery to insert from the box below.', 'tcu-gallery-plugin' ); ?></span>
			</div>

			<div class="section">
				<select name="tcu_gallery_name" id="tcugalleries_thickbox">
					<option value="-1">
						<?php esc_html_e( 'Select Gallery', 'tcu-gallery-plugin' ); ?>
					</option>
					<?php
					foreach ( $galleries as $gallery ) {
						echo "<option value=\"{$gallery['ID']}\">{$gallery['post_title']} (ID #{$gallery['ID']})</option>";
					}
					?>
				</select>
			</div>

			<div class="section">
				<button id="insert-gallery" class="button-primary" onClick="insertGallery();"><?php esc_html_e( 'Insert gallery', 'tcu-gallery-plugin' ); ?></button>
				<button id="close-gallery-thickbox" class="button-secondary" style="margin-left: 5px;" onClick="tb_remove();"><a><?php esc_html_e( 'Close', 'tcu-gallery-plugin' ); ?></a></button>
			</div>
		</div>
	<?php
	}

	/**
	 * Media button
	 *
	 * @param int $editor_id Editor ID.
	 */
	public function media_button( $editor_id ) {
	?>

		<style type="text/css">
			a.insert-gallery span.dashicons-feedback:before {
				content: "\f538";
				font: 400 16px/1 dashicons;
				speak: none;
				-webkit-font-smoothing: antialiased;
				-moz-osx-font-smoothing: grayscale;
			}
		</style>

		<a href="#TB_inline?width=480&amp;inlineId=select-tcu-gallery" class="button thickbox insert-gallery" data-editor="<?php echo esc_attr( $editor_id ); ?>" title="<?php echo esc_attr( 'Add gallery', 'tcu-gallery-plugin' ); ?>">
			<span class="dashicons dashicons-feedback"></span><?php esc_html_e( ' Add gallery', 'tcu-gallery-plugin' ); ?>
		</a>

	<?php
	}

	/**
	 * Set up image size for thumbnail
	 */
	public function set_new_image_size() {
		add_image_size( 'gallery_thumbnail', 750, 650, true );
		add_image_size( 'gallery_fullwidth', 1200, 800, false );
	}
}
