### TCU Gallery Plugin

## Author: Mayra Perales <mailto:m.j.perales@tcu.edu>

**v1.0.3**

-   Remove \_normalize.scss because it breaks the parent theme

**v1.0.2**

-   Remove \_layout.scss because it breaks the parent theme

**v1.0.1**

-   Fix typo in the Filters click event
-   Change styling for the Filter links

**v1.0.0**

-   initial release
