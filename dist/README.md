# TCU Gallery Plugin

Easily add a gallery into any page or widget area.

**GETTING STARTED**

We use GULP as our task runner. All our WordPress production files are located in dist/ directory. We also use a prefix of "tcu" within our CSS classes.

**Our Files**

We offer two versions — a minified version, and an un-minified one. Use the minified version in a production environment or to reduce the file size of your downloaded assets. And the un-minified version is better if you are in a development environment or would like to debug the CSS or JavaScript assets in the browser.

**Our main SASS files are**

    library/scss

**Working with an existing Gulp project**

Assuming that the Gulp CLI has been installed and that the project has already been configured with a package.json and a Gulpfile.js, it's very easy to start working with Gulp:

    - Change to the project's root directory.
    - Install project dependencies with npm install.
    - Run Gulp with gulp.

**Install NPM**

**[Download NPM](https://www.npmjs.com/)**

## Submit Bugs & or Fixes:

*   [Issues](https://bitbucket.org/TCUWebmanage/tcu-gallery-plugin)

## Meta

*   [Changelog](CHANGELOG.md)
