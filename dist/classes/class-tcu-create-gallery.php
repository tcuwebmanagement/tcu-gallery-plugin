<?php
/**
 * Create our gallery.
 *
 * @package tcu_gallery_plugin
 * @since TCU Gallery Plugin 1.0.0
 */

/**
 * Exit if accessed directly
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Registers our post type
 * Adds our custom meta boxes
 *
 * @author Mayra Perales <m.j.perales@tcu.edu>
 **/
class Tcu_Create_Gallery {

	/**
	 * Post type name
	 *
	 * @var string Post type name
	 */
	protected static $post_type_name = 'tcu_gallery';

	/**
	 * Taxonomy name
	 *
	 * @var string Taxonomy Name
	 */
	protected static $filters = 'tcu_gallery_filters';

	/**
	 * Taxonomy name
	 *
	 * @var string Taxonomy Name
	 */
	protected static $collections = 'tcu_gallery_collections';

	/**
	 * User capabilities
	 *
	 * @var array User capabilities
	 */
	private static $cap_aliases = array(
		// Full permissions to a post type.
		'editor'      => array(
			'read',
			'read_private_posts',
			'edit_posts',
			'edit_others_posts',
			'edit_private_posts',
			'edit_published_posts',
			'delete_posts',
			'delete_others_posts',
			'delete_private_posts',
			'delete_published_posts',
			'publish_posts',
		),
		'author'      => array(
			// Full permissions for content the user created.
			'read',
			'edit_posts',
			'edit_published_posts',
			'delete_posts',
			'delete_published_posts',
			'publish_posts',
		),
		'contributor' => array(
			// Create, but not publish.
			'read',
			'edit_posts',
			'delete_posts',
		),
		'subscriber'  => array(
			// Read only.
			'read',
		),
	);

	/**
	 * Our constructor
	 *
	 * @return void
	 */
	public function __construct() {

		// Register post type.
		add_action( 'init', array( &$this, 'register_gallery_post_type' ) );

		// Add user capabilities.
		add_action( 'wp_loaded', array( $this, 'set_user_caps' ) );

		// Replace table columns in edit.php.
		add_action( 'manage_tcu_gallery_posts_columns', array( $this, 'admin_columns' ), 10, 2 );

		// Add shortcode to table column in edit.php.
		add_action( 'manage_tcu_gallery_posts_custom_column', array( $this, 'shortcode_column' ), 10, 2 );
	}

	/**
	 * Returns post type name
	 *
	 * @return $post_type_name string The name of the custom post type
	 */
	public static function get_post_type_name() {
		return self::$post_type_name;
	}

	/**
	 * Returns Taxonomy name
	 *
	 * @return $filters string The name of the taxonomy
	 */
	public static function get_filters_name() {
		return self::$filters;
	}

	/**
	 * Returns Taxonomy name
	 *
	 * @return $filters string The name of the taxonomy
	 */
	public static function get_collections_name() {
		return self::$collections;
	}

	/**
	 * Register our Post Type
	 *
	 * @return void
	 **/
	public function register_gallery_post_type() {
		register_post_type(
			self::$post_type_name,
			// Let's now add all the options for this post type.
			array(
				'labels'              => array(
					'name'               => __( 'Gallery', 'tcu-gallery-plugin' ),
					'singular_name'      => __( 'Gallery', 'tcu-gallery-plugin' ),
					'all_items'          => __( 'All Galleries', 'tcu-gallery-plugin' ),
					'add_new'            => __( 'Add New', 'tcu-gallery-plugin' ),
					'add_new_item'       => __( 'Add New Gallery', 'tcu-gallery-plugin' ),
					'edit'               => __( 'Edit', 'tcu-gallery-plugin' ),
					'edit_item'          => __( 'Edit Gallery', 'tcu-gallery-plugin' ),
					'new_item'           => __( 'New Gallery', 'tcu-gallery-plugin' ),
					'view_item'          => __( 'View Gallery', 'tcu-gallery-plugin' ),
					'search_items'       => __( 'Search Galleries', 'tcu-gallery-plugin' ),
					'not_found'          => __( 'Nothing found in the Database.', 'tcu-gallery-plugin' ),
					'not_found_in_trash' => __( 'Nothing found in Trash', 'tcu-gallery-plugin' ),
				),
				'description'         => __( 'Easily add a gallery to any page.', 'tcu-gallery-plugin' ),
				'public'              => false,
				'has_archive'         => false,
				'publicly_queryable'  => false,
				'show_ui'             => true,
				'supports'            => array( 'title', 'author' ),
				'menu_position'       => 20,
				'menu_icon'           => 'dashicons-layout',
				'exclude_from_search' => true,
				'capability_type'     => array( 'tcu_gallery', 'tcu-galleries' ),
				'map_meta_cap'        => true,
			)
		); /* end of register post type */

		// Taxonomies.
		register_taxonomy(
			self::$filters, 'attachment',
			array(
				'labels'       => array(
					'name'              => __( 'Filters', 'tcu-gallery-plugin' ),
					'singular_name'     => __( 'Filter', 'tcu-gallery-plugin' ),
					'search_items'      => __( 'Search Filters', 'tcu-gallery-plugin' ),
					'all_items'         => __( 'All Filters', 'tcu-gallery-plugin' ),
					'parent_item'       => __( 'Parent Filter', 'tcu-gallery-plugin' ),
					'parent_item_colon' => __( 'Parent Filter:', 'tcu-gallery-plugin' ),
					'edit_item'         => __( 'Edit Filter', 'tcu-gallery-plugin' ),
					'update_item'       => __( 'Update Filter', 'tcu-gallery-plugin' ),
					'add_new_item'      => __( 'Add New Filter', 'tcu-gallery-plugin' ),
					'new_item_name'     => __( 'New Filter Name', 'tcu-gallery-plugin' ),
				),
				'hierarchical' => false,
				'show_ui'      => true,
				'public'       => false,
				'rewrite'      => false,
			)
		);

		register_taxonomy(
			self::$collections, 'attachment',
			array(
				'labels'       => array(
					'name'              => __( 'Collections', 'tcu-gallery-plugin' ),
					'singular_name'     => __( 'Collection', 'tcu-gallery-plugin' ),
					'search_items'      => __( 'Search Collections', 'tcu-gallery-plugin' ),
					'all_items'         => __( 'All Collections', 'tcu-gallery-plugin' ),
					'parent_item'       => __( 'Parent Collection', 'tcu-gallery-plugin' ),
					'parent_item_colon' => __( 'Parent Collection:', 'tcu-gallery-plugin' ),
					'edit_item'         => __( 'Edit Collection', 'tcu-gallery-plugin' ),
					'update_item'       => __( 'Update Collection', 'tcu-gallery-plugin' ),
					'add_new_item'      => __( 'Add New Collection', 'tcu-gallery-plugin' ),
					'new_item_name'     => __( 'New Collection Name', 'tcu-gallery-plugin' ),
				),
				'hierarchical' => false,
				'show_ui'      => true,
				'public'       => false,
				'rewrite'      => false,
			)
		);
	}

	/**
	 * Grant caps for the given post type to the given role
	 *
	 * @param string $post_type The post type to grant caps for.
	 * @param string $role_id The role receiving the caps.
	 * @param string $level The capability level to grant (see the list of caps above).
	 *
	 * @return bool false if the action failed for some reason, otherwise true
	 */
	public static function register_post_type_caps( $post_type, $role_id, $level = '' ) {
		if ( empty( $level ) ) {
			$level = $role_id;
		}

		if ( 'administrator' === $level ) {
			$level = 'editor';
		}

		if ( ! isset( self::$cap_aliases[ $level ] ) ) {
			return false;
		}

		$role = get_role( $role_id );
		if ( ! $role ) {
			return false;
		}

		$pto = get_post_type_object( $post_type );
		if ( empty( $pto ) ) {
			return false;
		}

		foreach ( self::$cap_aliases[ $level ] as $alias ) {
			if ( isset( $pto->cap->$alias ) ) {
					$role->add_cap( $pto->cap->$alias );
			}
		}

		return true;
	}


	/**
	 * Grant caps for the given post type
	 */
	public static function set_user_caps() {
		foreach ( array( 'administrator', 'editor', 'author', 'contributor', 'subscriber' ) as $role ) {
			self::register_post_type_caps( self::$post_type_name, $role );
		}
	}

	/**
	 * Remove all caps for the given post type from the given role
	 *
	 * @param string $post_type The post type to remove caps for.
	 * @param string $role_id The role which is losing caps.
	 *
	 * @return bool false If the action failed for some reason, otherwise true.
	 */
	public static function remove_post_type_caps( $post_type, $role_id ) {

		$role = get_role( $role_id );
		if ( ! $role ) {
			return false;
		}

		foreach ( $role->capabilities as $cap => $has ) {
			if ( strpos( $cap, $post_type ) !== false ) {
				$role->remove_cap( $cap );
			}
		}

		return true;
	}

	/**
	 * Remove capabilities for galleries and related post types from default roles
	 *
	 * @return void
	 */
	public static function remove_all_caps() {
		foreach ( array( 'administrator', 'editor', 'author', 'contributor', 'subscriber' ) as $role ) {
			self::remove_post_type_caps( self::$post_type_name, $role );
		}
	}

	/**
	 * Replace table columns in edit.php
	 *
	 * @param array $columns Array of columns in edit.php.
	 * @return array $columns Updated array of columns in edit.php
	 */
	public function admin_columns( $columns ) {

		$columns = array(
			'cb'                    => '<input type = "checkbox" />',
			'title'                 => __( 'Title', 'tcu-gallery-plugin' ),
			'author'                => __( 'Author', 'tcu-gallery-plugin' ),
			'tcu_gallery_shortcode' => __( 'Shortcode', 'tcu-gallery-plugin' ),
			'date'                  => __( 'Date', 'tcu-gallery-plugin' ),
		);

		return $columns;
	}

	/**
	 * Add timeline shortcode to table in edit.php
	 *
	 * @param  string $column   The name of the column to display.
	 * @param  int    $post_id  The ID of the current post.
	 */
	public function shortcode_column( $column, $post_id ) {
		global $typenow;

		if ( self::$post_type_name === $typenow ) {

			$shortcode = 'tcu_gallery_shortcode';

			if ( $column === $shortcode ) {

				echo wp_kses_post( '<code>[tcu_gallery id="' . $post_id . '"]</code>' );

			}
		}
	}

}

