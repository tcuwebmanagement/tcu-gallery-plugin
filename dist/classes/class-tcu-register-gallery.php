<?php
/**
 * Register plugin and loads functionality.
 *
 * @package tcu_gallery_plugin
 * @since TCU Gallery Plugin 1.0.0
 */

/**
 * Exit if accessed directly
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Registers plugin and loads functionality
 *
 * @author Mayra Perales <m.j.perales@tcu.edu>
 */
class Tcu_Register_Gallery {

	/**
	 * Our plugin version
	 *
	 * @var string Plugin version number.
	 */
	protected static $version = '1.0.2';

	/**
	 * Display Gallery Object
	 *
	 * @var object The Display Gallery Object
	 */
	protected $display_gallery;

	/**
	 * Create Gallery Object
	 *
	 * @var object The Create Gallery Object
	 */
	protected $create_gallery;

	/**
	 * Create Update Gallery Object
	 *
	 * @var object The pdate Gallery Object
	 */
	protected $update_gallery;

	/**
	 * Our constructor
	 */
	public function __construct() {

		// Loads all our include files.
		$this->load_files();

		// Instatiate Tcu_Create_Gallery.
		$this->create_gallery();

		// Instatiate Tcu_Display_Gallery.
		$this->display_gallery();

		// Instatiate Tcu_Update_Gallery.
		$this->update_gallery();

		// Activation and uninstall hooks.
		register_activation_hook( __FILE__, array( &$this, 'activate_me' ) );
		register_deactivation_hook( __FILE__, array( __CLASS__, 'uninstall_me' ) );

		// Load all our css and js files on front end side.
		add_action( 'wp_enqueue_scripts', array( $this, 'register_front_end_scripts' ), 999 );

		// Display warning when ACF plugin is not intalled.
		add_action( 'admin_notices', array( $this, 'acf_plugin_notice' ) );
	}

	/**
	 * Return plugin version
	 *
	 * @return string $version Plugin version
	 */
	public static function get_version() {
			return self::$version;
	}

	/**
	 * Instantiate Tcu_Create_Gallery
	 *
	 * @return object $create_gallery Gallery Custom Post Type
	 **/
	public function create_gallery() {
		if ( ! $this->create_gallery ) {
			$this->create_gallery = new Tcu_Create_Gallery();
		}
		return $this->create_gallery;
	}

	/**
	 * Instantiate Tcu_Display_Gallery
	 *
	 * @return object $display_gallery Gallery Display Object
	 **/
	public function display_gallery() {
		if ( ! $this->display_gallery ) {
			$this->display_gallery = new Tcu_Display_Gallery();
		}
		return $this->display_gallery;
	}

	/**
	 * Instantiate Tcu_Update_Gallery
	 *
	 * @return object $display_gallery Gallery Update Object
	 **/
	public function update_gallery() {
		if ( ! $this->update_gallery ) {
			$this->update_gallery = new Tcu_Update_Gallery();
		}
		return $this->update_gallery;
	}

	/**
	 * Activate our plugin
	 **/
	public function activate_me() {

		// Register Post Type.
		$this->create_gallery->register_gallery_post_type();

		// Set user capabilities.
		Tcu_Create_Gallery::set_user_caps();

		// Flush our rewrites.
		flush_rewrite_rules();
	}

	/**
	 * Deactivate plugin
	 */
	public function uninstall_me() {

		// Remove user capabilities.
		Tcu_Create_Timeline::remove_all_caps();

		// Flush our rewrites.
		flush_rewrite_rules();
	}

	/**
	 * Load all our dependencies
	 */
	public function load_files() {
		require_once TCU_GALLERY_DIRNAME . '/classes/class-tcu-display-gallery.php';
		require_once TCU_GALLERY_DIRNAME . '/classes/class-tcu-create-gallery.php';
		require_once TCU_GALLERY_DIRNAME . '/classes/class-tcu-update-gallery.php';
		require_once TCU_GALLERY_DIRNAME . '/library/inc/acf-gallery-fields.php';
		require_once TCU_GALLERY_DIRNAME . '/library/inc/acf-taxonomy-fields.php';
	}

	/**
	 * Register our front end scripts
	 * only if TCU Web Standards theme
	 * is not installed.
	 **/
	public function register_front_end_scripts() {

		wp_register_style( 'tcu-gallery-styles', plugins_url( 'library/css/style.min.css', dirname( __FILE__ ) ), array(), self::$version, 'all' );

		wp_register_script( 'tcu-gallery-scripts', plugins_url( 'library/js/min/gallery-scripts.min.js', dirname( __FILE__ ) ), array( 'jquery' ), self::$version, true );

		wp_register_script( 'lazy-loading-jquery', plugins_url( 'library/js/min/jquery.lazy.min.js', dirname( __FILE__ ) ), array( 'jquery' ), self::$version, true );

		wp_enqueue_style( 'tcu-gallery-styles' );
		wp_enqueue_script( 'lazy-loading-jquery' );
		wp_enqueue_script( 'tcu-gallery-scripts' );

	}

	/**
	 * Check if the ACF plugin is intalled
	 *
	 * @return bool  True if the ACF plugin is installed
	 */
	private function is_acf_installed() {

		/**
		 * Checks to see if "is_plugin_active" function exists and if not load the php file that includes that function */
		if ( ! function_exists( 'is_plugin_active' ) ) {
			include_once ABSPATH . 'wp-admin/includes/plugin.php';
		}

		// Checks to see if the acf pro plugin is activated.
		if ( is_plugin_active( 'advanced-custom-fields-pro/acf.php' ) || is_plugin_active( 'advanced-custom-fields/acf.php' ) ) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Admin notice to display when ACF is not installed
	 */
	public function acf_plugin_notice() {
		global $tcu_warning_notice;

		$tcu_warning_notice = esc_html__( 'The TCU Gallery plugin needs the "Advanced Custom Fields PRO" plugin to run. Please download and activate it.', 'tcu-gallery-plugin' );

		$allowed_tags = array(
			'div' => array(
				'class' => array(),
			),
		);

		if ( ! $this->is_acf_installed() ) {
			echo wp_kses( '<div class="notice notice-error notice-large"><div class="notice-title">' . $tcu_warning_notice . '</div></div>', $allowed_tags );
		}
	}
}
