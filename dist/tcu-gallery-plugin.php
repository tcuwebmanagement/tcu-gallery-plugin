<?php
/**
 * Plugin Name: TCU Gallery Plugin
 * Description: Easily add a gallery into any page through a shortcode.
 * Version: 1.0.3
 * Author: Website and Social Media Management
 * Author URI: http://mkc.tcu.edu/web-management.asp
 * Text Domain: tcu-gallery-plugin
 *
 * License: GNU General Public License v2.0
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html
 *
 * @package tcu_gallery_plugin
 * @since TCU Gallery Plugin 1.0.0
 */

/**
 * Exit if accessed directly
 **/
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

define( 'TCU_GALLERY_DIRNAME', dirname( __FILE__ ) );

// Register plugin.
require_once TCU_GALLERY_DIRNAME . '/classes/class-tcu-register-gallery.php';

// Let's get started!
if ( class_exists( 'Tcu_Register_Gallery' ) ) {
	new Tcu_Register_Gallery();
}


