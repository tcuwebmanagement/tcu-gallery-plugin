<?php
/**
 * ACF fields for our gallery.
 *
 * @package tcu_gallery_plugin
 * @since TCU Gallery Plugin 1.0.0
 */

/**
 * Exit if accessed directly
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

add_action( 'acf/init', 'tcu_gallery_fields' );

/**
 * Callback function for ACF/INIT action
 */
function tcu_gallery_fields() {

	acf_add_local_field_group(
		array(
			'key'                   => 'group_5ae7654a8e0bc',
			'title'                 => 'TCU Gallery Plugin',
			'fields'                => array(
				array(
					'key'               => 'field_5ae7655ff5618',
					'label'             => 'Add Images',
					'name'              => 'tcu_gallery_add_images',
					'type'              => 'gallery',
					'instructions'      => '',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'min'               => 1,
					'max'               => '',
					'insert'            => 'append',
					'library'           => 'all',
					'min_width'         => '',
					'min_height'        => '',
					'min_size'          => '',
					'max_width'         => '',
					'max_height'        => '',
					'max_size'          => '',
					'mime_types'        => '',
				),
				array(
					'key'               => 'field_5ae7836a8cc63',
					'label'             => 'Display Filters?',
					'name'              => 'tcu_gallery_display_filter',
					'type'              => 'true_false',
					'instructions'      => '',
					'required'          => 0,
					'conditional_logic' => 0,
					'wrapper'           => array(
						'width' => '',
						'class' => '',
						'id'    => '',
					),
					'message'           => '',
					'default_value'     => 1,
					'ui'                => 1,
					'ui_on_text'        => '',
					'ui_off_text'       => '',
				),
			),
			'location'              => array(
				array(
					array(
						'param'    => 'post_type',
						'operator' => '==',
						'value'    => 'tcu_gallery',
					),
				),
			),
			'menu_order'            => 0,
			'position'              => 'normal',
			'style'                 => 'default',
			'label_placement'       => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen'        => '',
			'active'                => 1,
			'description'           => '',
		)
	);

}
