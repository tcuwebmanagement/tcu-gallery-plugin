/*eslint wrap-iife: [2, "inside"]*/

// as the page loads, call these scripts
jQuery( document ).ready( function( $ ) {

    // Magnific Popup with gallery enabled
    $( '.tcu-popup-link--icon' ).magnificPopup( {
        type: 'image',
        gallery: {
            enabled: true
        }
    } );

    // Magnific Popup without gallery
    $( '.tcu-popup-link' ).magnificPopup( {
        type: 'image',
        gallery: {
            enabled: true
        }
    } );

    // Isotope
    var tcuGalleryIsotope = $( '.tcu-gallery-wrapper' ).isotope( {
        itemSelector: '.tcu-gallery-item',
        layoutMode: 'masonry',
        percentPosition: true,
        resize: true,
        filter: '*'
    } );

    // Bail if tcuGallery is empty.
    if ( ! tcuGalleryIsotope ) {
        return;
    }

    // Isotope layout
    if ( 'function' === typeof imagesLoaded ) {
        tcuGalleryIsotope.imagesLoaded().progress( function() {
            tcuGalleryIsotope.isotope( 'layout' );
        } );
    }

    // Lazy loading.
    $( '.tcu-gallery-lazy' ).lazy( {
        effect: 'fadeIn',
        effectTime: 2000,
        threshold: 0,
        afterLoad: function() {

            // Isotope layout
            if ( 'function' === typeof imagesLoaded ) {
                tcuGalleryIsotope.imagesLoaded().progress( function() {
                    tcuGalleryIsotope.isotope( 'layout' );
                } );
            }
        }
    } );

    // Flexslider
    $( '.tcu-gallery-slider' ).flexslider( {
        animation: 'slide',
        slideshowSpeed: 3000,
        init: function( slider ) {

            // lazy load
            $( slider )
                .find( 'img.tcu-slider-lazy' )
                .slice( 0, 5 )
                .each( function() {
                    var src = $( this ).attr( 'data-src' );
                    $( this )
                        .attr( 'src', src )
                        .removeAttr( 'data-src' );
                } );
        },
        start: function() {
            imagesLoaded( $( '.slides' ), function() {
                setTimeout( function() {
                    $( '.tcu-gallery-filter li:eq(0) a' ).trigger( 'click' );
                }, 1500 );
            } );
        },
        before: function( slider ) {

            // Fires asynchronously with each slider animation
            var slides = slider.slides,
                index = slider.animatingTo,
                $slide = $( slides[index] ),
                current = index + slider.cloneOffset,
                nxtSlide = current + 1,
                prevSlide = current - 1;
            $slide
                .parent()
                .find(
                    'img.tcu-slider-lazy:eq(' +
                        current +
                        '), img.tcu-slider-lazy:eq(' +
                        prevSlide +
                        '), img.tcu-slider-lazy:eq(' +
                        nxtSlide +
                        ')'
                )
                .each( function() {
                    var src = $( this ).attr( 'data-src' );
                    $( this )
                        .attr( 'src', src )
                        .removeAttr( 'data-src' );
                } );
        }
    } );

    // Add Magnific Popup to slider
    $( '.tcu-gallery-slider' ).each( function() {

        // the containers for all your galleries
        var _items = $( this ).find( '.slides li:not(.clone) > a' );
        var items = [];
        for ( var i = 0; i < _items.length; i++ ) {
            items.push( { src: $( _items[i] ).attr( 'href' ), title: $( _items[i] ).attr( 'title' ) } );
        }

        $( this )
            .parent()
            .find( '.tcu-action-btn' )
            .magnificPopup( {
                items: items,
                type: 'image',
                gallery: {
                    enabled: true
                }
            } );
        $( this )
            .parent()
            .find( '.tcu-gallery-description' )
            .magnificPopup( {
                items: items,
                type: 'image',
                gallery: {
                    enabled: true
                }
            } );
    } );

    // Start Filtering with Isotope
    // Let's make sure we only show a 'gallery' of the filtered items
    $( '.tcu-gallery-filter a' ).on( 'click', function( event ) {
        event.preventDefault();

        // Find item with tcu-gallery-current and remove.
        $( '.tcu-gallery-current' ).removeClass( 'tcu-gallery-current' );

        // Add tcu-gallery-current to the LI
        $( this )
            .parent()
            .addClass( 'tcu-gallery-current' );

        // Our filter data attribute
        var selector = $( this ).data( 'filter' );

        tcuGalleryIsotope.isotope( { filter: $( this ).data( 'filter' ) } );

        // click on filtered items - icon
        $( selector )
            .find( '.tcu-popup-link--icon' )
            .magnificPopup( {
                type: 'image',
                gallery: {
                    enabled: true
                }
            } );

        // click on filtered items -  title
        $( selector )
            .find( '.tcu-popup-link' )
            .magnificPopup( {
                type: 'image',
                gallery: {
                    enabled: true
                }
            } );
    } );

    // Select the filter heading
    var filterButton = $( '.tcu-filter-heading' );
    var filterMenu = $( '.tcu-gallery-filter' );

    filterButton.click( function() {

        // Toggle aria-expanded
        if ( 'false' === $( this ).attr( 'aria-expanded' ) ) {
            $( this )
                .attr( 'aria-expanded', 'true' )
                .html( 'Filters <span class="tcu-visuallyhidden">Contract Menu</span><span> &#8722; </span>' );
        } else {
            $( this )
                .attr( 'aria-expanded', 'false' )
                .html( 'Filters <span class="tcu-visuallyhidden">Expand Menu</span><span> + </span>' );
        }

        // Toggle slide
        filterMenu.slideToggle();
    } );

    // Close menu when ESC key is pressed
    window.addEventListener( 'keydown', function( e ) {

        // If ESC key is pressed
        if ( 'Escape' === e.code && 27 === e.keyCode ) {
            filterButton
                .attr( 'aria-expanded', 'false' )
                .attr( 'tabindex', '0' )
                .html( 'Filters <span class="tcu-visuallyhidden">Expand Menu</span><span> + </span>' );

            // Toggle slide
            filterMenu.css( 'display', 'none' );
        }
    } );

    var screenWidth = window.innerWidth || document.documentElement.clientWidth;
    var maxScreenWidth = 700;

    // If mobile screen then collapse filter menu
    if ( screenWidth < maxScreenWidth ) {
        filterMenu.css( 'display', 'none' );
        filterButton
            .attr( 'aria-expanded', 'false' )
            .html( 'Filters <span class="tcu-visuallyhidden">Expand Menu</span><span> + </span>' );
    }

    // On window resize
    $( window ).resize( function() {
        var currentWidth = window.innerWidth || document.documentElement.clientWidth;

        // If mobile screen then collapse filter menu
        if ( currentWidth < maxScreenWidth ) {
            filterMenu.css( 'display', 'none' );
            filterButton
                .attr( 'aria-expanded', 'false' )
                .html( 'Filters <span class="tcu-visuallyhidden">Expand Menu</span><span> + </span>' );
        } else {
            filterMenu.css( 'display', 'flex' );
            filterButton
                .attr( 'aria-expanded', 'true' )
                .html( 'Filters <span class="tcu-visuallyhidden">Contract Menu</span><span> + </span>' );
        }
    } );
} ); /* end of as page load scripts */
